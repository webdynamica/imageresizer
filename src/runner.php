<?php

namespace Webdynamica\ImageResizer;

use Gumlet\ImageResize;

class Runner
{
    protected $skip = ['.', '..'];

    function __construct()
    {

    }

    private function renameFile($file, $folder, $new_folder)
    {
        $file_name = pathinfo($file, PATHINFO_FILENAME);
        $file_extension = pathinfo($file, PATHINFO_EXTENSION);

        $new_file['name'] = $file_name;
        $new_file['name'] = preg_replace('/,/i', '', $new_file['name']);
        $new_file['name'] = str_replace(' ', '-', $new_file['name']);
        $new_file['name'] = strtolower($new_file['name']);
        $new_file['extension'] = strtolower($file_extension);

        $new_filename = $new_file['name'] . '.' . $new_file['extension'];

        if (!file_exists($new_folder)) {
            mkdir($new_folder, 0755, true);
        }

        $old_file = $folder . DIRECTORY_SEPARATOR . $file;
        $new_file = $new_folder . DIRECTORY_SEPARATOR . $new_filename;
        if (file_exists($new_file)) {
            if (filemtime($old_file) > filemtime($new_file)) {
                copy($old_file, $new_file);
            }
        } else {
            copy($old_file, $new_file);
        }

        return $new_filename;
    }

    /**
     * Resize the image
     *
     * @return mixed
     */

    private function resizeFile($file, $folder)
    {
        $new_folder = $folder . DIRECTORY_SEPARATOR . '../../assets/images/album';
        $thumbnails_folder = $new_folder . DIRECTORY_SEPARATOR . 'thumbs';

        if (!file_exists($thumbnails_folder)) {
            mkdir($thumbnails_folder, 0755, true);
        }

        $new_file = $this->renameFile($file, $folder, $new_folder);
        $new_file_path = $new_folder . DIRECTORY_SEPARATOR . $new_file;
        $thumb_file_path = $thumbnails_folder . DIRECTORY_SEPARATOR . $new_file;

        if (!file_exists($thumbnails_folder . DIRECTORY_SEPARATOR . $new_file)) {
            $image = new ImageResize($new_file_path);
            $image->crop(300, 200, true, ImageResize::CROPCENTER);
            $image->save($thumb_file_path);
        } else {
            if (filemtime($new_file_path) > filemtime($thumb_file_path)) {
                $image = new ImageResize($new_file_path);
                $image->crop(300, 200, true, ImageResize::CROPCENTER);
                $image->save($thumb_file_path);
            }
        }

        return [
            'file' => $new_file_path,
            'thumbnail' => $thumb_file_path
        ];
    }

    public function getFiles($folder)
    {
        $files = [];

        if ($handle = opendir($folder)) {
            while (false !== ($file = readdir($handle))) {
                if (!in_array($file, $this->skip)) {
                    $file = $this->resizeFile($file, $folder);

                    $files[] = $file;
                }
            }
        }

        return $files;
    }
}
